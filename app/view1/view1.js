'use strict';

angular.module('myApp.view1', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/view1', {
            templateUrl: 'view1/view1.html',
            controller: 'View1Ctrl'
        });
    }])

    .controller('View1Ctrl', ['$scope', '$http', function ($scope, $http) {
        var URL = "http://localhost:8080";
        var self = this;
        this.$scope = $scope;
        $scope.books = [];
        self.books = [];

        self.getBooks = function () {
            $http.get(URL + "/books")
                .then(
                    function (odpowiedz) { // ta funkcja wykona sie jesli bedzie sukces
                        self.books = odpowiedz.data;
                        $scope.books = odpowiedz.data;
                    },
                    function (odpowiedzWPrzypadkBledu) { // ta funkcja jesli bedzie blad

                    });
        };

        self.getBooks();

        console.log("sadasd" + self.books);
    }]);