'use strict';

angular.module('myApp.viewLogin', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/viewLogin', {
            templateUrl: 'viewLogin/viewLogin.html',
            controller: 'ViewLoginCtrl'
        });
    }])

    .controller('ViewLoginCtrl', ['$http', '$routeParams', '$window', 'AuthService',
        function ($http, $routeParams, $window, AuthService) {
            var URL = "http://localhost:8080";
            var self = this;

            this.loginUser = {
                'login': '',
                'password': ''
            };

            this.sendLoginForm = function () {
                $http.post(URL + '/auth/authenticate', self.loginUser)
                    .then(
                        function (odpowiedz) {
                            //jeśli się uda
                            console.log(odpowiedz);

                            var token = odpowiedz.data.token;

                            $http.defaults.headers.common['Authorization'] = 'Bearer ' + token;
                        },
                        function (odpowiedzKiedyBlad) {
                            // nie udało się
                            console.log(odpowiedzKiedyBlad);
                        });
            }
        }]);