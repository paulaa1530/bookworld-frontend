'use strict';

angular.module('myApp.addBook', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/addBook', {
            templateUrl: 'addBookView/addBook.html',
            controller: 'addBookController'
        });
    }])

    .controller('addBookController', ['$http', '$routeParams', '$window', 'AuthService', function ($http, $routeParams, $window, AuthService) {
        var URL = "http://localhost:8080";
        var self = this;
        self.authorList = [];
        self.book = {};
        self.book.categoryList = [];
        self.categories =[];

        this.getAuthorList = function () {
            $http.get(URL + "/authors")
                .then(function (response) {
                        self.authorList = response.data;
                    },
                    function (odpowiedzNaBlad) {
                        $window.location = "#!/view1";
                    });
        };

        this.getPublisherList = function () {
            $http.get(URL + "/publishers")
                .then(function (response) {
                        self.publisherList= response.data;
                    },
                    function (odpowiedzNaBlad) {
                        $window.location = "#!/view1";
                    });
        };

        this.getCategoryList = function () {
            $http.get(URL + "/categories")
                .then(function (response) {
                    self.categories= response.data;
                },
                function (odpowiedzNaBlad) {
                    $window.location = "#!/view1";
                });

        };

        self.updateCategory =function(){
            self.book.categoryList = [];
            var category = {};
            category.id= self.chosenCategory;
            self.book.categoryList.push(category);
        };

        self.getAuthorList();

        self.createUser = function(){
            $http.post(URL + '/books',self.book) .then(function (response) {
                    console.log(response);
                    $window.location = "#!/view1";
                },
                function (odpowiedzNaBlad) {
                });
    }
        self.getPublisherList();
        self.getCategoryList();
    }]);